**Welcome to the European Marginal Mix Tool !**

To help researchers to identify the marginal countries in the Europe grid and calculate marginal emission factors

The tool comprises:
* aaaa
* bbb



**Preview**

Figure 1:
![](image/Figure1.png)



**Release history**
*  v0 : 11/07/2024


**License**

Licensed by La Rochelle University/LaSIE under a BSD 3 license (https://opensource.org/licenses/BSD-3-Clause).


**References**

TBD



